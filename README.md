1926 StVA
=========

OSM-Karte auf Basis der Karte »Stadtvermessungsamt Weimar 1926« im Stadtarchiv Weimar.

## Quellen für die Karte Weimar 1926

### Hauptquelle: Karte »Stadtvermessungsamt Weimar 1926« (1:4000 hausgenau)

Manuldruck von F. Ullmann G. m. b. H., Zwickau SA.

#### 4 Einzelblätter, montiert auf Holzrahmen, stark nikotingeschädigt
+ Stadtarchiv Weimar, Benutzerraum

#### 3 Einzelblätter, Druck auf verschiedenem Papier
+ Stadtarchiv Weimar 70 1/975
+ Stadtarchiv Weimar 70 1/628
+ Stadtarchiv Weimar 70 1/904

#### 4 Einzelblätter
+ Stadtarchiv Weimar, Nachlass Paul Werschy

Der 1926er Plan bietet eine perfekte Grundlage zur Darstellung der Städtebaulichen Situation zu dieser Zeit in Weimar. Zum Teil gibt es größere Abweichungen beim Abstand von Häuserblöcken im Vergleich zur modernen Liegenschaftskarte. Innerhalb eines Blockes ist die Karte jedoch zumeist recht genau ausgeführt. Dies legt nahe, dass sie aus einzelnen Kartenwerken zusammengeführt worden ist. Quellen sind wohl die genaueren Kartenblätter, zumeist »Stadtbauplan« betitelt. Hinsichtlich der Hausnummern ist die Karte sehr Lückenhaft und kann nur im Zusammenhang mit dem Adressbuch 1926 zur präzisen Zuordnung genutzt werden.


### Kartenblätter Alfred Ingber 1908, Stadtarchiv Weimar 70 ?

Druck unbekannter Methode. Probleme mit Verzerrungen des Blattmaterials, sowie scheinbar vor der Reproduktion enstandende, starke Verzerrungen insbesondere auf Blatt 4. Sehr hilfreich zur Identifikation von Hausnummern. Mitunter aber auch deutliche Abweichungen in der Nummerierung im Vergleich zu 1926.

Im Vergleich zur 1926er Karte fällt auf, dass besonders Hinterhofgebäude oft invertiert koloriert sind, also Gebäude und Freiraum vertauscht. Auch in der 1926er Karte sind die Hinterhofgebäude nicht immer korrekt koloriert und so schwer von Höfen und freien Plätzen zu unterscheiden. Meistens lassen sich diese Fehler durch ältere Stadtbaupläne ausräumen.

Eine ältere Version dieser Karte von 1890 existiert im Stadtmuseum Weimar.


### Karten »Stadtbauplan …«

Diese Karten sind wesentlich genauer als der 1926er Plan ausgeführt und bieten für viele Gebäude, welche im aktuellen ALK nicht mehr referenzierbar sind, eine recht genaue Grundlage. Insbesondere der Bereich des Asbachgrünzuges und der Jakobsvorstadt profitiert in der Genauigkeit sehr von diesen Plänen.

+ 1909, Stadtarchiv Weimar 70 1090, »Stadtbauplan von Weimar. Nördlicher Teil südlich der Bahn.« (1:1000 hausgenau, gezeichnet durch Albert (?) Thriemer)
+ 1912, Stadtarchiv Weimar 70 1076 »Stadtbauplan von Weimar. Innerer Teil.« (1:1000 hausgenau, gezeichnet durch M. Müller (mglw. Max [Buchdruckereibesitzer] oder Moritz [Revisionsassistent])
+ weitere noch zu nennende Pläne aus dieser Reihe


### Karten »Grundriß …« und vergleichbare Viertelübersichten

+ 1884 Stadtarchiv 70 1/39
+ 1892 Stadtarchiv 70 1/1084
+ 1896 Stadtarchiv 70 1/(369–415 zwischen?) (»Grundriß …«, Ingber)
+ 1896 Stadtarchiv 70 1/813
+ 1898 Stadtarchiv 70 1/38 (»Grundriß …«, Ingber)
+ 1873 Stadtarchiv 70 1/760
+ 1900 (?) Stadtarchiv 70 1/833
+ 1900 ca. Stadtarchiv 70 1/49
+ 1900 Stadtarchiv 70 1/146 (2)


### Weitere Karten, wohl auf den Stadtbauplänen basierend

+ 1913, Stadtarchiv 70 1/1083, »Baufluchtlinienplan für Kettenberg« (Jakobsvorstadt bis Museum und Carl-August-Platz), (1:1000 hausgenau, unterzeichnet »Müller 11.8.13«)
+ 1936, LATh – HStA Weimar, Thüringisches Finanzministerium Nr. 3711 Bl. 1 KatAbbB09, Abbruchplan mit den Messpunkten von H. Dietsch vom 24. Oktober 1936


### Modernere Karten nach 1945

+ 1952 Stadtarchiv 70 1/991 (2)
+ 1989 Stadtarchiv 70 1/1357, 1358 und weitere (1:500)
+ 2012 (ca.) offizielle ALK

### Einwohnerbuch der Stadt Weimar und ihres Wirtschaftskreises

Weimar: Dietsch & Brückner, 1926, Stadtarchiv Weimar

#### Zusätzliche OSM-artige Key-Value-Paare für Daten aus dem Adressbuch Weimar

+ `owner` = Der unter »E« verzeichnetet Eigentümer des Hauses
+ `caretaker` = Der unter »V« verzeichnetet Verwalter des Hauses
+ `brandkatasternummer` = Brandkatasternummer
+ `inhabitant:floor:-1` = Bewohner, Kellergeschoss (»K«)
+ `inhabitant:floor:0` = Bewohner, Erdgeschoss (»P«)
+ `inhabitant:floor:1` = Bewohner, erster Stock, etc. (»I«)
+ `inhabitant:floor:mansard` = Bewohner, Mansardgeschoss, etc. (»M«)
+ `inhabitant:back:floor:1` = Bewohner Hintergebäude, erster Stock, etc. (»H I«)
+ `inhabitant:annex:floor:1` = Bewohner Seitengebäude, erster Stock, etc. (»S I«)

### Weitere Quellen

+ »Kulturdenkmale in Thüringen«, Band 4, Weimar, ISBN 978-3-937940-54-0

+ »Weimar Brühl 8–16, Ehemalige Ofenfabrik Schmidt«, Bauhistorische Untersuchung Mai/Juni 2011, Wittwar Tomaschek Büro für Bauforschung in der Denkmalpflege

+ »Wasser unter der Stadt, Stadthygiene in Weimar vom Mittelalter bis zum 20. Jahrhundert« Abwasserbetrieb Weimar 2012

### Danke

+ Jens Riederer, Leiter des Stadtarchivs Weimar
+ Bernward Fechtel, Stellvertretender Amtsleiter, Stadtverwaltung Weimar, Stadtentwicklungsamt, Abt. Geoinformation und Statistik

--------------------------

## Mögliche zukünftige weitere Kartenvarianten

+ 1569 (nach Wolf, ?)
+ 1786/1800 (nach Lossius, KSW)
+ 1822 (nach Blaufuß, ThHStAW)
+ 1885 (nach Mueller)
+ 1890 (nach Ingber)
+ 1908 (nach Ingber)
+ 1945 (nach StVA, Luftbildern, Plan 1952)
+ 1989 (nach VEB Geodäsie und Kartographie Erfurt)


--------------------------


## OSM-Daten 1926 —> pgsql

    osm2pgsql -U alex --slim -c -G -d stvma1926 -S ~/Weimar/1926\ OSM/1926.style ~/Weimar/1926\ OSM/1926.osm

## OSM Daten Geofabrik —> pgsql

http://download.geofabrik.de/europe/germany/thueringen.html

    osm2pgsql -U alex -c -G -d osm -S /usr/local/share/osm2pgsql/default.style ~/Downloads/thueringen-latest.osm.pbf
    
## OSM —> GeoJSON

https://github.com/tyrasd/osmtogeojson

    osmtogeojson 1926.osm > 1926.geojson
    
## Auf 6 Dezimalstellen reduzieren (= ~11cm)
(scheint beim späteren Reduzieren in tippecanoe keinen Unterschied zu machen)
https://github.com/jczaplew/geojson-precision

    geojson-precision -p 6 1926.geojson 1926.geojson

## GeoJSON —> Vector Tiles

https://openmaptiles.org/docs/generate/custom-vector-from-shapefile-geojson/    
    
    tippecanoe -d 13 -z19 --force -o 1926.mbtiles 1926.geojson
    
Filter features:

    tippecanoe -d 13 -z19 --force -x 'source:start_date' -x user -x uid -x changeset -x version -x timestamp -o 1926.mbtiles 1926.geojson
    
